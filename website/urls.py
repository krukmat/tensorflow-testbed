"""composeexample URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from backend.views import CompanyViewSet, CategoryViewSet, RewardViewSet, SocialAccountViewSet, ActivateAccount, \
    RewardsXUserViewSet, SubscriptionViewSet, get_coins_top_list, get_what_to_mine, get_mining_equipment

admin.autodiscover()

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'companies', CompanyViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'rewards', RewardViewSet)
router.register(r'rewardsxuser', RewardsXUserViewSet)
router.register(r'social_accounts', SocialAccountViewSet)
router.register(r'subscription', SubscriptionViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^admin/', admin.site.urls),
    url(r'^activate/$', ActivateAccount.as_view()),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # User management
    url(r'^auth/', include('djoser.urls.authtoken')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'top_coins/', get_coins_top_list),
    url(r'mining_equipment/', get_mining_equipment),
    url(r'what_to_mine/', get_what_to_mine)
]