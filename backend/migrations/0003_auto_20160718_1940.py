# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0002_auto_20160712_0124'),
    ]

    operations = [
        migrations.AddField(
            model_name='mypointsuser',
            name='address',
            field=models.CharField(max_length=255, null=True, verbose_name='address'),
        ),
        migrations.AddField(
            model_name='mypointsuser',
            name='city',
            field=models.CharField(max_length=255, null=True, verbose_name='city'),
        ),
        migrations.AddField(
            model_name='mypointsuser',
            name='country',
            field=models.CharField(max_length=255, null=True, verbose_name='country'),
        ),
        migrations.AddField(
            model_name='mypointsuser',
            name='first_name',
            field=models.CharField(max_length=255, null=True, verbose_name='first_name'),
        ),
        migrations.AddField(
            model_name='mypointsuser',
            name='last_name',
            field=models.CharField(max_length=255, null=True, verbose_name='last_name'),
        ),
        migrations.AddField(
            model_name='mypointsuser',
            name='phone',
            field=models.CharField(max_length=255, null=True, verbose_name='phone'),
        ),
    ]
