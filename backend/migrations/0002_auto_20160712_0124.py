# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reward',
            name='codigo',
            field=models.CharField(default=b'', max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='reward',
            name='concept',
            field=models.CharField(default=b'', max_length=255, null=True),
        ),
    ]
