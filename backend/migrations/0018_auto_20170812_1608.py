# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0017_company_share_reward_points'),
    ]

    operations = [
        migrations.AddField(
            model_name='rewardxuser',
            name='shared_in_social',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='subscription',
            name='share_social',
            field=models.BooleanField(default=False),
        ),
    ]
